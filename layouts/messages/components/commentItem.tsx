import {
  Avatar,
  Box,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@mui/material";
import Link from "next/link";
import React from "react";
import useSWR from "swr";
import { string } from "yup/lib/locale";
import dateConverter from "../../../src/dateConverter";
import fetcher from "../../../src/fetcher";
import { Comment } from "../../../types/interfaces";

interface CommentCardProps {
  comment: Comment;
}

const CommentCard = ({ comment }: CommentCardProps) => {
  console.log(
    "🚀 ~ file: commentItem.tsx ~ line 15 ~ CommentCard ~ comment",
    comment
  );
  const { data, error } = useSWR(`/api/users/${comment.owner}`, fetcher);
  const isLoading = !data && !error;

  return (
    <>
      {isLoading ? (
        <></>
      ) : (
        <ListItem alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt={data.username} src={data.profile_pic} />
          </ListItemAvatar>
          <ListItemText
            primary={
              <Link href={`/users/${data.id}`}>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    marginTop: 0.5,
                  }}
                >
                  <Typography>
                    <a>{data.name ? data.name : data.username}</a>
                  </Typography>
                </Box>
              </Link>
            }
            secondary={
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  marginTop: 0.5,
                }}
              >
                <Typography
                  sx={{ display: "inline" }}
                  variant="body2"
                  color="text.primary"
                >
                  {comment.body}
                </Typography>
                {dateConverter(comment.updated_at)}
              </Box>
            }
          />
        </ListItem>
      )}
    </>
  );
};

export default CommentCard;
