import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  Skeleton,
  Typography,
} from "@mui/material";
import Link from "next/link";
import React from "react";
import useSWR from "swr";
import AvatarIcon from "../../../components/avatarIcon";
import dateConverter from "../../../src/dateConverter";
import fetcher from "../../../src/fetcher";
import { Post } from "../../../types/interfaces";

interface MessageCardProps {
  isLoading?: boolean;
  post: Post;
}

const MessageCard = ({ isLoading, post }: MessageCardProps) => {
  const { data, error } = useSWR(() => `/api/users/${post!.owner}`, fetcher);
  const isLoad = isLoading || (!data && !error);

  return (
    <Card
      elevation={0}
      sx={{
        minWidth: 275,
        border: 1,
        borderColor: "grey.300",
        my: 4,
      }}
    >
      <Link href={isLoad ? "" : `/profile/${data.username}`}>
        <CardHeader
          sx={[
            {
              "&:hover": {
                cursor: "pointer",
              },
            },
          ]}
          avatar={
            isLoad ? (
              <Skeleton
                animation="pulse"
                variant="circular"
                width={40}
                height={40}
              />
            ) : (
              <AvatarIcon profile={data} width={48} />
            )
          }
          title={
            isLoad ? (
              <Skeleton
                height={10}
                animation="pulse"
                width="50%"
                sx={{ marginBottom: 0.5 }}
              />
            ) : (
              data.name ? data.name : data.username
            )
          }
          subheader={
            isLoad ? (
              <Skeleton height={10} animation="pulse" width="30%" />
            ) : (
              "@" + data.username
            )
          }
        />
      </Link>
      <Divider />
      <CardContent>
        <CardContent
          sx={{
            px: 2.5,
          }}
        >
          <Typography
            variant="h3"
            sx={{
              fontWeight: "medium",
            }}
          >
            {isLoad ? <Skeleton animation="pulse" width="50%" /> : post?.title}
          </Typography>
          <Typography sx={{ mb: 1.5 }} color="text.secondary">
            {isLoad ? (
              <Skeleton animation="pulse" width="30%" />
            ) : (
              dateConverter(post?.updated_at)
            )}
          </Typography>
          {isLoad ? (
            <Skeleton animation="pulse" variant="rectangular" height={118} />
          ) : (
            <Typography variant="body1" sx={{}}>
              {post?.body}
            </Typography>
          )}
        </CardContent>
      </CardContent>
    </Card>
  );
};

export default MessageCard;
