import {
  Avatar,
  Box,
  Container,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@mui/material";
import React from "react";
import useSWR from "swr";
import fetcher from "../../src/fetcher";
import { Comment, Post } from "../../types/interfaces";
import CommentCard from "./components/commentItem";
import MessageCard from "./components/messageCard";

interface MessagesLayoutProps {
  post: Post;
}

const MessagesLayout = ({ post }: MessagesLayoutProps) => {
  const { data, error } = useSWR(`/api/posts/${post.id}/comments`, fetcher);
  const isLoading = !data && !error;
  const comments: Comment[] = data

  return (
    <Container maxWidth="lg">
      <Box
        sx={{
          my: 5,
        }}
      >
        <MessageCard post={post} />
        <Typography
          variant="h4"
          sx={{
            fontSize: 24,
          }}
        >
          Komentar
        </Typography>
        <List
          sx={{
            width: "100%",
            marginTop: 1,
            bgcolor: "background.paper",
            border: 1,
            borderColor: "grey.300",
          }}
        >
          {!Boolean(comments) ? (
            <></>
          ) : (
            comments.map((comment) => {
              return (
                <div key={toString(comment.id)}>
                  <CommentCard comment={comment} />
                </ div>
              );
            })
          )}
        </List>
      </Box>
    </Container>
  );
};

export default MessagesLayout;
