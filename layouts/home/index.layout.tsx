import { Box, Toolbar, Typography } from "@mui/material";
import CardPost from "../../components/cardPost";
import MessageForm from "../../components/messageForm";
import { Post } from "../../types/interfaces";

const IndexLayout = (props: { isLoading: boolean; posts: Post[] }) => {
  return (
    <Box
      sx={{
        flexGrow: 1,
      }}
    >
      <MessageForm />
      {props.isLoading ? (
        <CardPost isLoading />
      ) : (
        props.posts.map((post) => {
          return <div key={post.id}><CardPost post={post} /></div>;
        })
      )}
    </Box>
  );
};

export default IndexLayout;
