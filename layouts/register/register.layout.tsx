import { Box, Stack, Typography } from "@mui/material";
import React from "react";
import RegisterForm from "./components/registerForm";

const RegisterLayout = () => {
  return (
    <Box
      sx={{
        p: 8,
      }}
    >
      <Stack spacing={3}>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            gap: 1,
          }}
        >
          <Typography variant="h4">
            Selamat Datang di <br />
            <b>Message Board</b>
          </Typography>
          <Typography color={"primary.dark"}>Daftarkan akun anda</Typography>
        </Box>
        <Box>
          <RegisterForm />
        </Box>
      </Stack>
    </Box>
  );
};

export default RegisterLayout;
