import * as yup from "yup";
import { useFormik } from "formik";
import React from "react";
import { Box, Button, Stack, TextField, Typography } from "@mui/material";

const validationSchema = yup.object({
  username: yup
    .string()
    .min(3, "Minimal 3 karakter")
    .max(12, "Maksimal 12 karakter")
    .matches(
      /^[a-zA-Z0-9_]+$/,
      "Hanya huruf, angka, dan underscore `_` yang diperbolehkan "
    )
    .required("Username dibutuhkan"),
  email: yup
    .string()
    .min(3, "Minimal 3 karakter")
    .email("Masukkan Email yang valid!")
    .required("Email dibutuhkan"),
  firstname: yup.string().required("Nama depan dibutuhkan"),
  lastname: yup.string(),
  password: yup
    .string()
    .min(8, "Password minimal 8 huruf!")
    .required("Password dibutuhkan"),
});

const RegisterForm = () => {
  const formik = useFormik({
    initialValues: {
      username: "",
      email: "",
      firstname: "",
      lastname: "",
      password: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });
  return (
    <Box sx={{ width: "100%" }}>
      <form onSubmit={formik.handleSubmit}>
        <Stack spacing={4}>
          <Stack
            component="form"
            sx={{
              width: "100%",
            }}
            spacing={3}
            noValidate
            autoComplete="off"
          >
            <TextField
              fullWidth
              id="username"
              name="username"
              label="Username*"
              onChange={formik.handleChange}
              error={formik.touched.username && Boolean(formik.errors.username)}
              helperText={formik.touched.username && formik.errors.username}
              variant="standard"
            />
            <Box
              sx={{
                display: "flex",
                gap: 3
              }}
            >
              <TextField
                fullWidth
                id="firstname"
                name="firstname"
                label="Nama Depan*"
                onChange={formik.handleChange}
                error={
                  formik.touched.firstname && Boolean(formik.errors.firstname)
                }
                helperText={formik.touched.firstname && formik.errors.firstname}
                variant="standard"
              />
              <TextField
                fullWidth
                id="lastname"
                name="lastname"
                label="Nama Belakang"
                onChange={formik.handleChange}
                error={
                  formik.touched.lastname && Boolean(formik.errors.lastname)
                }
                helperText={formik.touched.lastname && formik.errors.lastname}
                variant="standard"
              />
            </Box>
            <TextField
              fullWidth
              id="email"
              name="email"
              label="Email*"
              onChange={formik.handleChange}
              error={formik.touched.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
              variant="standard"
            />
            <TextField
              fullWidth
              id="password"
              name="password"
              label="Password*"
              type="password"
              onChange={formik.handleChange}
              error={formik.touched.password && Boolean(formik.errors.password)}
              helperText={formik.touched.password && formik.errors.password}
              variant="standard"
            />
          </Stack>
          <Button color="primary" variant="contained" fullWidth type="submit">
            Daftar
          </Button>
        </Stack>
      </form>
    </Box>
  );
};

export default RegisterForm;
