import { Box, Card, Container } from "@mui/material";
import React from "react";
import Masonry from "@mui/lab/Masonry";
import type { Profile } from "../../types/interfaces";
import ProfileCard from "./components/profileCard";

interface AllProfileProps {
  users: Profile[];
}

const AllProfileLayout = ({ users }: AllProfileProps) => {
  return (
    <Container maxWidth="lg">
      <Box
        sx={{
          marginTop: 5,
        }}
      >
        <Masonry columns={3} spacing={3}>
          {users.map((user) => {
            return (
              <Box key={user.username}>
                <ProfileCard user={user} />
              </Box>
            );
          })}
        </Masonry>
      </Box>
    </Container>
  );
};

export default AllProfileLayout;
