import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Typography,
} from "@mui/material";
import React from "react";
import { Profile } from "../../../types/interfaces";
import AvatarIcon from "../../../components/avatarIcon";
import Link from "next/link";

interface ProfileCardProps {
  user: Profile;
}

const ProfileCard = ({ user }: ProfileCardProps) => {
  const profileName = (user.name ? user.name : user.username);

  return (
    <Card>
      <Box
        display={"flex"}
        sx={{
          width: "100%",
          justifyContent: "center",
          my: 3,
        }}
      >
        <AvatarIcon profile={user} width={100} />
      </Box>
      <CardContent
        sx={{
          textAlign: "center",
        }}
      >
        <Typography variant="h5" sx={{ fontWeight: "500" }}>
          {profileName}
        </Typography>
        <Typography sx={{ fontSize: 14, fontWeight: "200" }}>
          @{user.username}
        </Typography>
        <Box sx={{ marginTop: 1 }}>
          <Typography>{user.bio}</Typography>
        </Box>
      </CardContent>
      <CardActions
        sx={{
          display: "flex",
          p: 2,
          justifyContent: "center",
        }}
      >
        <Link href={`/profile/${user.username}`}>
          <Button>Selengkapnya</Button>
        </Link>
      </CardActions>
    </Card>
  );
};

export default ProfileCard;
