import React, { useState } from "react";
import { useRouter } from 'next/router'
import * as yup from "yup";
import { useFormik } from "formik";
import {
  Alert,
  Box,
  Button,
  FormControl,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { signIn } from "next-auth/react";
import type { SignInResponse } from "next-auth/react";

const validationSchema = yup.object({
  email: yup
    .string()
    .min(3, "Minimal 3 karakter")
    .email('Masukkan email yang valid')
    .required("E-mail dibutuhkan"),
  password: yup
    .string()
    .min(3, "Password minimal 3 karakter")
    .required("Password dibutuhkan"),
});

const LoginForm = () => {
  const router = useRouter()

  const [isError, setisError] = useState(false);
  const handleError = () => {
    setisError(true);
  };

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: validationSchema,
    onSubmit: async (values) => {
      signIn("credentials", { ...values, redirect: false }).then((r) => {
        let res: SignInResponse = r!;

        if (Boolean(res.error)) {
          handleError();
        } else {
          router.push('/')
        }
      });
    },
  });
  return (
    <Box sx={{ display: "flex", flexWrap: "wrap" }}>
      <form onSubmit={formik.handleSubmit}>
        <Stack spacing={4}>
          <Typography variant="h4">
            Selamat Datang <b>Kembali</b>
          </Typography>
          <Stack
            sx={{
              width: "50ch",
            }}
            spacing={2}
          >
            {isError ? (
              <Alert severity="warning">
                Username dan/atau Password anda salah!
              </Alert>
            ) : (
              <></>
            )}
            <TextField
              fullWidth
              id="email"
              name="email"
              label="E-mail*"
              onChange={formik.handleChange}
              error={formik.touched.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
              variant="standard"
            />
            <TextField
              fullWidth
              id="password"
              name="password"
              label="Password*"
              type="password"
              onChange={formik.handleChange}
              error={formik.touched.password && Boolean(formik.errors.password)}
              helperText={formik.touched.password && formik.errors.password}
              variant="standard"
            />
          </Stack>
          <Button color="primary" variant="contained" fullWidth type="submit">
            Login
          </Button>
        </Stack>
      </form>
    </Box>
  );
};

export default LoginForm;
