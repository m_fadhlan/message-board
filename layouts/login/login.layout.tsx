import { Box, Card } from "@mui/material";
import React from "react";
import LoginForm from "./components/loginForm";

const LoginLayout = () => {
  return (
    <Box
      sx={{
        minHeight: "100vh",
        display: 'flex'
      }}
    >
      <Card elevation={4} sx={{
        margin: 'auto',
        p: 8
      }}>
        <LoginForm />
      </Card>
    </Box>
  );
};

export default LoginLayout;
