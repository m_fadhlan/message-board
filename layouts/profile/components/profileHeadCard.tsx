import {
  Box,
  Button,
  Card,
  CardActions,
  IconButton,
  Paper,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import * as yup from "yup";
import { useFormik } from "formik";
import { Profile } from "../../../types/interfaces";
import Avatar from "boring-avatars";
import { HiCog } from "@react-icons/all-files/hi/HiCog";
import AvatarIcon from "../../../components/avatarIcon";

interface ProfileProps {
  profile: Profile;
}

const validationSchema = yup.object({
  username: yup
    .string()
    .min(3, "Minimal 3 karakter")
    .max(12, "Maksimal 12 karakter")
    .matches(
      /^[a-zA-Z0-9_]+$/,
      "Hanya huruf, angka, dan underscore `_` yang diperbolehkan "
    )
    .required("Username dibutuhkan"),
  bio: yup.string().min(3, "Minimal 3 karakter"),
  name: yup.string(),
});

const ProfileHeadCard = ({ profile }: ProfileProps) => {
  const [isEditing, setisEditing] = useState(false);
  const handleEditing = () => {
    setisEditing(true);
  };
  const handleCloseEditing = (
    profile?: { username: string; name: string; bio?: string } | undefined
  ) => {
    setisEditing(false);
    if (Boolean(profile)) {
      // handle profile update
    }
  };

  const formik = useFormik({
    initialValues: {
      username: profile.username,
      name: profile.name,
      bio: profile.bio,
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleCloseEditing(values);
      alert(JSON.stringify(values, null, 2));
    },
  });

  const profileName = profile.name ? profile.name : profile.username;

  return (
    <Card
      sx={{
        p: 3,
        display: "flex",
        backgroundColor: "background.paper",
        gap: 4,
      }}
    >
      {isEditing ? (
        <>
          <Box
            sx={{
              height: "40%",
            }}
          >
            <AvatarIcon profile={profile} width={70} />
          </Box>
          <form
            onSubmit={formik.handleSubmit}
            style={{
              display: "flex",
              width: "100%",
            }}
          >
            <Stack
              sx={{
                flexGrow: 1,
              }}
              gap={1}
            >
              <Box>
                <Box
                  sx={{
                    display: "flex",
                    gap: 3,
                  }}
                >
                  <TextField
                    id="name"
                    name="name"
                    label="Nama"
                    placeholder={profile.name}
                    onChange={formik.handleChange}
                    error={formik.touched.name && Boolean(formik.errors.name)!}
                    helperText={formik.touched.name && formik.errors.name}
                    variant="standard"
                    InputProps={{
                      style: {
                        fontFamily: "Barlow Condensed",
                        fontSize: 24,
                      },
                    }}
                  />
                </Box>
                <TextField
                  id="username"
                  name="username"
                  label="Username*"
                  placeholder={profile.username}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.username && Boolean(formik.errors.username)
                  }
                  helperText={formik.touched.username && formik.errors.username}
                  variant="standard"
                  size="small"
                  sx={{
                    marginTop: 1,
                  }}
                  InputProps={{
                    style: {
                      fontFamily: "Open Sans",
                      fontSize: 15,
                    },
                  }}
                />
              </Box>
              <TextField
                id="bio"
                name="bio"
                type="bio"
                label="Bio"
                placeholder={profile.bio}
                onChange={formik.handleChange}
                error={formik.touched.bio && Boolean(formik.errors.bio)}
                helperText={formik.touched.bio && formik.errors.bio}
                multiline
                rows={4}
                variant="standard"
                InputProps={{
                  style: {
                    fontFamily: "Open Sans",
                  },
                }}
              />
            </Stack>
            <CardActions>
              <Stack gap={2}>
                <Button size="small" variant="contained" disabled>
                  Simpan
                </Button>
                <Button
                  size="small"
                  variant="outlined"
                  onClick={() => handleCloseEditing()}
                >
                  Tutup
                </Button>
              </Stack>
            </CardActions>
          </form>
        </>
      ) : (
        <>
          <Box>
            <AvatarIcon profile={profile} width={150} />
          </Box>
          <Stack
            sx={{
              flexGrow: 1,
            }}
            gap={1}
          >
            <Box>
              <Typography variant="h4">{profileName}</Typography>
              <Typography>@{profile.username}</Typography>
            </Box>
            <Typography>{profile.bio}</Typography>
          </Stack>
        </>
      )}

      {isEditing ? (
        <></>
      ) : (
        <CardActions>
          <IconButton disableFocusRipple onClick={() => handleEditing()}>
            <HiCog />
          </IconButton>
        </CardActions>
      )}
    </Card>
  );
};

export default ProfileHeadCard;
