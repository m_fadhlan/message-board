import { Box, Container } from "@mui/material";
import React from "react";
import useSWR from "swr";
import CardPost from "../../components/cardPost";
import MessageForm from "../../components/messageForm";
import fetcher from "../../src/fetcher";
import type { Profile } from "../../types/interfaces";
import ProfileHeadCard from "./components/profileHeadCard";

interface ProfileProps {
  profile: Profile;
}

const ProfileLayout = ({ profile }: ProfileProps) => {
  const { data, error } = useSWR(`/api/users/${profile.id}/posts`, fetcher);
  const isLoading = !data && !error;
  console.log("🚀 ~ file: profile.layout.tsx ~ line 17 ~ ProfileLayout ~ data", data)

  return (
    <Container maxWidth="lg">
      <Box
        sx={{
          marginTop: 5,
        }}
      >
        <ProfileHeadCard profile={profile} />
        <MessageForm />
        {isLoading ? (
          <CardPost isLoading />
        ) : (data.length !== 0 ? 
          data.map((post) => {
            return <CardPost post={post} />;
          }) : <></>
        )}
      </Box>
    </Container>
  );
};

export default ProfileLayout;
