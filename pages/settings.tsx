import type { NextPage } from "next";
import React from "react";
import MainContainer from "../components/mainContainer";
import SettingsLayout from "../layouts/settings/settings.layout";

const Settings: NextPage = () => {
  return (
    <>
      <MainContainer>
        <SettingsLayout />
      </MainContainer>
    </>
  );
};

export default Settings;
