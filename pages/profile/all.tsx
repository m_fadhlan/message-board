import { GetServerSidePropsContext } from "next";
import { getSession } from "next-auth/react";
import getConfig from "next/config";
import React from "react";
import MainContainer from "../../components/mainContainer";
import AllProfileLayout from "../../layouts/all-profile/allProfile.layout";
import type { Profile } from "../../types/interfaces";

interface AllProfileProps {
  users: Profile[];
}

const AllProfile = ({ users }: AllProfileProps) => {
  return (
    <MainContainer>
      <AllProfileLayout users={users} />
    </MainContainer>
  );
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const { publicRuntimeConfig } = getConfig();
  const url = publicRuntimeConfig.apiUrl;

  const session = await getSession(context);
  if (!session) {
    return {
      redirect: {
        destination: "/login",
        permanent: false,
      },
    };
  }

  const res = await fetch(`${url}/users/`);
  const data: Profile[] = await res.json();

  return {
    props: { session, users: data },
  };
}

export default AllProfile;
