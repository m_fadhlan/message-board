import { GetServerSidePropsContext } from "next";
import { getSession } from "next-auth/react";
import getConfig from "next/config";
import React from "react";
import MainContainer from "../../components/mainContainer";
import ProfileLayout from "../../layouts/profile/profile.layout";
import { Profile } from "../../types/interfaces";

interface ProfileProps {
  user: Profile;
}

const Profile = ({ user }: ProfileProps) => {
  return (
    <>
      <MainContainer>
        <ProfileLayout profile={user} />
      </MainContainer>
    </>
  );
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const username = context.query.username;
  const { publicRuntimeConfig } = getConfig();
  const url = publicRuntimeConfig.apiUrl;

  const session = await getSession(context);
  if (!session) {
    return {
      redirect: {
        destination: "/login",
        permanent: false,
      },
    };
  }

  const res = await fetch(`${url}/users?username=${username}`);
  const data = await res.json();

  if(data.status === 404) {
      return {
          notFound: true,
      }
  }
  
  return {
    props: { session, user: data },
  };
}

export default Profile;
