import { Box } from "@mui/material";
import type { GetServerSidePropsContext, NextPage } from "next";
import { getSession } from "next-auth/react";
import React from "react";
import GradientBackground from "../../components/gradientBackground";
import RegisterLayout from "../../layouts/register/register.layout";

const Register: NextPage = () => {
  return (
    <GradientBackground>
      <Box
        sx={{
          display: "flex",
          minHeight: "100vh",
          flexDirection: "row-reverse",
        }}
      >
        <Box
          sx={{
            backgroundColor: "common.white",
            minHeight: "100%",
            width: "70%",
          }}
        >
          <RegisterLayout />
        </Box>
      </Box>
    </GradientBackground>
  );
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const session = await getSession(context)

  if (session) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
  
  return {
    props: { session }
  }
}

export default Register;
