import type { NextApiRequest, NextApiResponse } from "next";
import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";

async function signInServer(email: String, password: String) {
  const url = process.env.API_URL;
  const res = await fetch(`${url}/users/login/`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    body: JSON.stringify({
      email: email,
      password: password,
    }),
  });  

  if (res.ok) {
    return await res.json();
  } else {
    return null;
  }
}

export default (req: NextApiRequest, res: NextApiResponse) =>
  NextAuth(req, res, {
    providers: [
      CredentialsProvider({
        name: "Credentials",
        credentials: {
          email: {
            label: "Email",
            type: "text",
            placeholder: "email",
          },
          password: { label: "Password", type: "password" },
        },
        authorize: async (credentials) => {
          const res = await signInServer(
            credentials!.email,
            credentials!.password
          );

          if (Boolean(await res.data)) {
            return {email: res.data.email, id: res.data.id, name: res.data.token};
          } else {
            return null;
          }
        },
      }),
    ],
    pages: {
      signIn: "/login",
    },
    jwt: {
      maxAge: 9999,
    },
    callbacks: {
      jwt: ({ token, user }) => {
        // first time jwt callback is run, user object is available
        if (user) {
          token.id = user.id;
        }
        return token;
      },

      session: ({ session, token }) => {
        if (token) {
          session.id = token.id;
        }
        return session;
      },
    },
  });
