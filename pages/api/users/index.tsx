import type { NextApiRequest, NextApiResponse } from "next";
import type { Profile } from "../../../types/interfaces";

async function getUsers(){
  const url = process.env.API_URL;
  const res = await fetch(`${url}/users/`)
  if(res.ok){
    return await res.json()
  } else {
    return null
  }
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const users = await getUsers()
  const username = req.query.username as string;

  if (!Boolean(users)) {
    return res.status(404).json({
      status: 404,
      message: "Data Not Found",
    });
  }

  const result = users.data.results

  if(Boolean(username)){
    const user = result.find(user => user.username === username)

    if (!Boolean(user)) {
      return res.status(404).json({
        status: 404,
        message: "Data Not Found",
      });
    }
    return res.status(200).json(user)
  }

  res.status(200).json(result);
};
