import type { NextApiRequest, NextApiResponse } from "next";

async function getUser(id: string) {
  const url = process.env.API_URL;
  const res = await fetch(`${url}/users/${id}/`);
  if (res.ok) {
    return await res.json();
  } else {
    return null;
  }
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const id = req.query.id as string;

  const user = await getUser(id);

  if (!Boolean(user)) {
    return res.status(404).json({
      status: 404,
      message: "Data Not Found",
    });
  }
  res.status(200).json(user.data);
};
