import type { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/react";

async function getPosts() {
  const url = process.env.API_URL;
  const res = await fetch(`${url}/posts/`);
  if (res.ok) {
    return await res.json();
  } else {
    return null;
  }
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req });
  const id = req.query.id as string;

  if (session) {
    const posts = await getPosts();
    const post = posts.data.results.filter(
      (post) => post.owner === parseInt(id)
    );

    if (!Boolean(post)) {
      return res.status(404).json({
        status: 404,
        message: "Data Not Found",
      });
    }
    res.status(200).json(post);
  }
};
