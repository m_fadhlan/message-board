import type { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/react";
import * as postData from "../../../../data/posts.json";

async function getPost(req:NextApiRequest ,id: string) {
  const session = await getSession({req});
  const token = session!.user!.name;

  const url = process.env.API_URL;
  const res = await fetch(`${url}/posts/${id}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      "Authorization": `Token ${token}`
    },
  });

  if(res.ok){
    return await res.json()
  } else {
    return null
  }
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const id = req.query.id as string;

  const post = await getPost(req, id)

  if (!Boolean(post)) {
    return res.status(404).json({
      status: 404,
      message: "Data Not Found",
    });
  }
  res.status(200).json(post.data);
};
