import type { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/react";

async function getComments(postId: string){
  const url = process.env.API_URL;
  const res = await fetch(`${url}/posts/${postId}/comments`);

  if(res.ok){
    return await res.json()
  } else {
    return null
  }
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
   const session = await getSession({ req });
   const postId = req.query.id as string

  if (session) {
    const comments = await getComments(postId)

    if (!Boolean(comments)) {
      return res.status(404).json({
        status: 404,
        message: "Data Not Found",
      });
    }
    res.status(200).json(comments.data.results);
  } else {
    res.status(401).json({
      status: 401,
      message: "Unathorized Access",
    });
  }
}