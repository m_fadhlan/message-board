import type { NextApiRequest, NextApiResponse } from "next";

async function getPosts(){
  const url = process.env.API_URL;
  const res = await fetch(`${url}/posts/`)
  if(res.ok){
    return await res.json()
  } else {
    return null
  }
}

export default async (req: NextApiRequest, res: NextApiResponse) => {

  const posts = await getPosts()

  if (!Boolean(posts)) {
    return res.status(404).json({
      status: 404,
      message: "Data Not Found",
    });
  }
  res.status(200).json(posts.data.results);
};
