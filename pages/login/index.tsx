import type { GetServerSidePropsContext, NextPage } from 'next'
import { getSession } from 'next-auth/react'
import React from 'react'
import GradientBackground from '../../components/gradientBackground'
import LoginLayout from '../../layouts/login/login.layout'

const Login: NextPage = () => {
  return (
    <GradientBackground>
      <LoginLayout />
    </GradientBackground>
  )
}

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const session = await getSession(context)

  if (session) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
  
  return {
    props: { session }
  }
}

export default Login