import * as React from "react";
import type { GetServerSidePropsContext, NextPage } from "next";
import IndexLayout from "../layouts/home/index.layout";
import MainContainer from "../components/mainContainer";
import { getSession, useSession } from "next-auth/react";
import useSWR from "swr";
import fetcher from "../src/fetcher";

const Home: NextPage = () => {
  const { data: session, status } = useSession()
  const { data, error } = useSWR('/api/posts', fetcher)

  const isLoading = ((status === "loading") || (!data && !error))

  return (
    <>
      <MainContainer>
        <IndexLayout isLoading={isLoading} posts={data} />
      </MainContainer>
    </>
  );
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const session = await getSession(context)

  if (!session) {
    return {
      redirect: {
        destination: '/login',
        permanent: false,
      },
    }
  }

  return {
    props: { session }
  }
}

export default Home;
