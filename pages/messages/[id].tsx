import { Box, CircularProgress } from "@mui/material";
import { GetServerSidePropsContext } from "next";
import { getSession, useSession } from "next-auth/react";
import getConfig from "next/config";
import { useRouter } from "next/router";
import React from "react";
import useSWR from "swr";
import MainContainer from "../../components/mainContainer";
import MessagesLayout from "../../layouts/messages/messages.layout";
import fetcher from "../../src/fetcher";
import type { Post } from "../../types/interfaces";

interface MessagesProps {
  post: Post;
}

const Messages = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: session, status } = useSession();
  const { data, error } = useSWR(`/api/posts/${id}`, fetcher);

  const isLoading = (status === "loading") || (!data && !error);

  return (
    <MainContainer>
      {isLoading ? (
        <Box
          sx={{
            marginTop: 4,
          }}
        >
          <CircularProgress />
        </Box>
      ) : (
        <MessagesLayout post={data} />
      )}
    </MainContainer>
  );
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const session = await getSession(context);
  if (!session) {
    return {
      redirect: {
        destination: "/login",
        permanent: false,
      },
    };
  }

  return {
    props: { session },
  };
}

export default Messages;
