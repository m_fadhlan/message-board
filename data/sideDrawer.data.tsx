import type { IconType } from "@react-icons/all-files/lib";
import { ReactElement } from "react";

import { HiHome } from "@react-icons/all-files/hi/HiHome";
import { HiUserGroup } from "@react-icons/all-files/hi/HiUserGroup";
import { HiUser } from "@react-icons/all-files/hi/HiUser";
import { HiCog } from "@react-icons/all-files/hi/HiCog";

const SideDrawerData: { name: string; logo: ReactElement<IconType> }[] = [
  {
    name: "Beranda",
    logo: <HiHome />,
  },
  {
    name: "Pengguna",
    logo: <HiUserGroup />,
  },
  {
    name: "Profile",
    logo: <HiUser />,
  },
  {
    name: "Pengaturan",
    logo: <HiCog />,
  },
];

export default SideDrawerData;
