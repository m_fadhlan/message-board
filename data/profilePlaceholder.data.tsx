import { Profile } from "../types/interfaces";

const profileFadhlan: Profile = {
  username: "fadhln",
  avatarUrl: "photo_2021-02-11_16-04-43_m53yis.jpg",
  firstname: "Fadhlan",
  lastname: "Muhammad",
  email: "fadhlan1337@gmail.com",
  bio: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi molestias perspiciatis deserunt architecto sapiente, id aperiam quia beatae voluptas iusto.",
  post: [
    {
      id: 1,
      title: "Post kesatu",
      createdAt: new Date(),
      authorId: 1,
      content:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo voluptate mollitia optio eaque quasi tenetur eos. Sequi iste culpa doloremque voluptates modi commodi voluptatum voluptatem, autem dolorum suscipit, assumenda vitae?",
    },
    {
      id: 2,
      title: "Post kedua",
      createdAt: new Date(),
      authorId: 2,
      content:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo voluptate mollitia optio eaque quasi tenetur eos. Sequi iste culpa doloremque voluptates modi commodi voluptatum voluptatem, autem dolorum suscipit, assumenda vitae?",
    },
  ],
};

export default profileFadhlan;