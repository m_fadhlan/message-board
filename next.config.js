/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["res.cloudinary.com"],
  },
  publicRuntimeConfig: {
    apiUrl:
      process.env.NODE_ENV === "development"
        ? "http://localhost:3000/api" // development url
        : "http://localhost:3000/api", // production url
  },
  env: {
    API_URL: "https://prosaintern-mp1.herokuapp.com/api"
  }
};
