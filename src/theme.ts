import { createTheme, ThemeOptions } from "@mui/material";


const theme = createTheme({
  palette: {
    primary: {
      main: "#b2dfdb",
      light: "#e5ffff",
      dark: "#82ada9",
    },
    secondary: {
      main: "#ffab91",
      light: "#ffddc1",
      dark: "#c97b63",
    },
  },
  typography: {
    fontFamily: "Open Sans",
    h6: {
      fontFamily: "Barlow Condensed",
    },
    h5: {
      fontFamily: "Barlow Condensed",
    },
    h4: {
      fontFamily: "Barlow Condensed",
    },
    h3: {
      fontFamily: "Barlow Condensed",
    },
    h2: {
      fontFamily: "Barlow Condensed",
    },
    h1: {
      fontFamily: "Barlow Condensed",
    },
    button: {
      fontWeight: 700,
    },
  },
  components: {
    MuiSwitch: {
      styleOverrides: {
        root: {
          width: 42,
          height: 26,
          padding: 0,
          margin: 8,
        },
        switchBase: {
          padding: 1,
          "&$checked, &$colorPrimary$checked, &$colorSecondary$checked": {
            transform: "translateX(16px)",
            color: "#fff",
            "& + $track": {
              opacity: 1,
              border: "none",
            },
          },
        },
        thumb: {
          width: 24,
          height: 24,
        },
        track: {
          borderRadius: 13,
          border: "1px solid #bdbdbd",
          backgroundColor: "#fafafa",
          opacity: 1,
          transition:
            "background-color 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,border 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
        },
      },
    },
  },
});

export default theme