export default function dateConverter(dateString: string | undefined) {
  let date: Date;
  
  if (!Boolean(dateString)) {
    date = new Date();
  } else {
    date = new Date(dateString);
  }

  const options = {
    weekday: "long",
    year: "numeric",
    month: "short",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
  };

  return date.toLocaleString("id-ID", options);
}
