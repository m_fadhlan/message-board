
// to be implemented
export interface Post {
    id: number,
    title: string,
    body: string,
    createdAt: string,
    id_user: number,
    [key: string]: any
}

export interface Profile {
    id: number,
    username: string,
    profile_pic?: string,
    email: string,
    name: string,
    bio: string,
}

export interface Comment {
    id: number,
    body: string,
    updated_at: string,
    owner: number,
    post: number
}