import {
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Toolbar,
  Typography,
} from "@mui/material";

import { FC } from "react";
import { Profile } from "next-auth";

import { HiHome } from "@react-icons/all-files/hi/HiHome";
import { HiUserGroup } from "@react-icons/all-files/hi/HiUserGroup";
import { HiUser } from "@react-icons/all-files/hi/HiUser";
import { HiCog } from "@react-icons/all-files/hi/HiCog";
import Link from "next/link";

interface SideDrawerProps {
  width: number;
  isLoading?: boolean
  profile?: Profile;
}

const SideDrawer: FC<SideDrawerProps> = ({ width, profile, isLoading }) => {
  const SideDrawerData = [
    {
      name: "Beranda",
      logo: <HiHome />,
      href: "/"
    },
    {
      name: "Pengguna",
      logo: <HiUserGroup />,
      href: `/profile/all`
    },
    {
      name: "Profile",
      logo: <HiUser />,
      href: (isLoading ? `/profile/all` : `/profile/${profile!.username}`)
    },
  ];
  return (
    <Drawer
      sx={{
        width: width,
        flexShrink: 0,
        borderColor: "grey.300",
        "& .MuiDrawer-paper": {
          width: width,
          boxSizing: "border-box",
        },
      }}
      variant="permanent"
      anchor="left"
    >
      <Toolbar
        sx={{
          backgroundColor: "primary.main",
        }}
      >
        <Typography
          variant="h5"
          noWrap
          component="div"
          sx={{
            color: "common.white",
            fontWeight: "light",
            flexGrow: 1,
            textAlign: "center",
          }}
        >
          Message Board <b>App</b>
        </Typography>
      </Toolbar>
      <List>
        {SideDrawerData.map((content, index) => (
          <Link key={index.toString()} href={content.href}>
            <ListItem button>
              <ListItemIcon
                sx={{
                  fontSize: 24,
                  color: "grey.600",
                  mx: "auto",
                }}
              >
                {content.logo}
              </ListItemIcon>
              <ListItemText primary={content.name} />
            </ListItem>
          </Link>
        ))}
      </List>
    </Drawer>
  );
};

export default SideDrawer;
