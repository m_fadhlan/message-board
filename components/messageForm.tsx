import {
  Box,
  Button,
  ClickAwayListener,
  Collapse,
  Paper,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import Router from "next/router";
import { useFormik } from "formik";
import * as yup from "yup";
import { signOut, useSession } from "next-auth/react";
import { LoadingButton } from "@mui/lab";

const ValidationSchema = yup.object({
  title: yup
    .string()
    .min(3, "Masukkan minimal 3 karakter")
    .max(60, "Maksimal 60 karakter!")
    .required("Masukkan judul!"),
  body: yup
    .string()
    .min(3, "Masukkan minimal 3 karakter")
    .max(140, "Maksimal 140 karakter!")
    .required("Masukkan pesan!"),
});

const MessageForm = () => {
  const { data: session, status } = useSession();
  const [isFocused, setFocused] = useState(false);
  const [isSubmitting, setSubmitting] = useState(false);

  const handleClick = () => {
    setFocused(true);
  };

  const handleClickAway = () => {
    setFocused(false);
  };

  const formik = useFormik({
    initialValues: {
      title: "",
      body: "",
    },
    validationSchema: ValidationSchema,
    onSubmit: async (values) => {
      setSubmitting(true);
      const token = session?.user?.name;
      const url = process.env.API_URL;
      const resp = await fetch(`${url}/posts/`, {
        method: "POST",
        mode: "cors",
        keepalive: true,
        headers: {
          Authorization: `Token ${token}`,
          Accept: "application/json, text/plain, */*",
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(values),
      }).then((res) => {
        setSubmitting(false);
        if (res.ok) {
          console.log(res.status + " -OK- " + JSON.stringify(values) + ` token ${token}`);
        } else if (res.status === 401) {
          signOut();
        } else {
          alert(res.status + " " + res.statusText);
        }
      });
    },
  });

  return (
    <Box
      sx={{
        marginTop: 3,
      }}
    >
      <ClickAwayListener onClickAway={handleClickAway}>
        <Paper
          elevation={0}
          sx={{
            minWidth: 275,
            border: 1,
            borderColor: "grey.300",
            my: 4,
            p: 2.5,
          }}
        >
          <Collapse in={isFocused} collapsedSize={65}>
            <form onSubmit={formik.handleSubmit}>
              <Stack gap={2}>
                <TextField
                  id="title"
                  name="title"
                  type="title"
                  label={isFocused ? "Judul" : "Tuliskan Pesan"}
                  value={formik.values.title}
                  onChange={formik.handleChange}
                  error={formik.touched.title && Boolean(formik.errors.title)}
                  helperText={formik.touched.title && formik.errors.title}
                  variant="standard"
                  onFocus={() => handleClick()}
                  InputProps={{
                    style: {
                      fontFamily: "Barlow Condensed",
                      fontSize: 24,
                    },
                  }}
                />
                <TextField
                  id="body"
                  name="body"
                  type="body"
                  label="Pesan"
                  value={formik.values.body}
                  onChange={formik.handleChange}
                  error={formik.touched.body && Boolean(formik.errors.body)}
                  helperText={formik.touched.body && formik.errors.body}
                  multiline
                  rows={4}
                  variant="standard"
                  InputProps={{
                    style: {
                      fontFamily: "Open Sans",
                    },
                  }}
                />
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row-reverse",
                    width: "100%",
                  }}
                  gap={2}
                >
                  <LoadingButton
                    variant="contained"
                    type="submit"
                    loading={isSubmitting}
                  >
                    Kirim
                  </LoadingButton>
                  <Button variant="outlined" onClick={() => handleClickAway()}>
                    Tutup
                  </Button>
                </Box>
              </Stack>
            </form>
          </Collapse>
        </Paper>
      </ClickAwayListener>
    </Box>
  );
};

export default MessageForm;
