import {
  Avatar,
  Divider,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Tooltip,
} from "@mui/material";
import { signOut } from "next-auth/react";
import React, { useState } from "react";
import profileFadhlan from "../data/profilePlaceholder.data";
import AvatarIcon from "./avatarIcon";
import { HiLogout } from "@react-icons/all-files/hi/HiLogout";
import { HiCog } from "@react-icons/all-files/hi/HiCog";
import Link from "next/link";
import { Profile } from "../types/interfaces";
import { HiUserGroup } from "@react-icons/all-files/hi/HiUserGroup";

interface AvatarMenuProps {
  profile: Profile;
}

const AvatarMenu = ({ profile }: AvatarMenuProps) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const avatarSize = 35;

  const handleClick = (e: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(e.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Tooltip title="Pengaturan Profil">
        <IconButton
          onClick={handleClick}
          size="small"
          sx={{ ml: 2 }}
          aria-controls={open ? "account-menu" : undefined}
          aria-haspopup="true"
          aria-expanded={open ? "true" : undefined}
        >
          <Avatar sx={{ background: "transparent" }}>
            <AvatarIcon width={avatarSize} profile={profile} />
          </Avatar>
        </IconButton>
      </Tooltip>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <Link href={`/profile/${profile.username}`}>
          <MenuItem>
            <Avatar sx={{ background: "transparent" }}>
              <AvatarIcon width={avatarSize} profile={profile} />
            </Avatar>
            Profile
          </MenuItem>
        </Link>
        <Divider />
        <Link href={`/profile/all`}>
        <MenuItem>
          <ListItemIcon>
            <HiUserGroup />
          </ListItemIcon>
          Semua Pengguna
        </MenuItem>
        </Link>
        <MenuItem onClick={() => signOut()}>
          <ListItemIcon>
            <HiLogout fontSize="small" />
          </ListItemIcon>
          Logout
        </MenuItem>
      </Menu>
    </>
  );
};

export default AvatarMenu;
