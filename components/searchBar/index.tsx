import Search from "./styles/search.style";
import SearchIcon from "@mui/icons-material/Search";
import SearchIconWrapper from "./styles/searchIconWrapper.style";
import StyledInputBase from "./styles/styledInputBase.style";

const SearchBar = () => {
  return (
    <>
      <Search>
        <SearchIconWrapper>
          <SearchIcon />
        </SearchIconWrapper>
        <StyledInputBase
          placeholder="Search…"
          inputProps={{ "aria-label": "search" }}
        />
      </Search>
    </>
  );
};

export default SearchBar;
