import React, { FC } from "react";
import {
  AppBar,
  Box,
  Container,
  Skeleton,
  Toolbar,
} from "@mui/material";
// import SearchAppBar from "./searchBar";
import AvatarMenu from "./avatarMenu";
import { useSession } from "next-auth/react";
import { Profile } from "../types/interfaces";

interface NavBarProps {
  leftMargin: number;
  isLoading?: boolean
  profile?: Profile
}

const NavBar: FC<NavBarProps> = ({ leftMargin, isLoading, profile }) => {

  return (
    <>
      <AppBar
        position="fixed"
        elevation={0}
        sx={{
          backgroundColor: "common.white",
          width: `calc(100% - ${leftMargin}px)`,
          marginLeft: `${leftMargin}px`,
          borderBottom: 1,
          borderColor: "grey.400",
        }}
      >
        <Container maxWidth="lg">
          <Toolbar>
            {/* <SearchAppBar /> */}
            <Box sx={{ flexGrow: 1 }} />
            {isLoading ? <Skeleton
                animation="pulse"
                variant="circular"
                width={40}
                height={40}
              /> : <AvatarMenu profile={profile!} />}
          </Toolbar>
        </Container>
      </AppBar>
    </>
  );
};

export default NavBar;
