import { alpha, Box, Container } from "@mui/material";
import { useSession } from "next-auth/react";
import React, { FC } from "react";
import useSWR from "swr";
import fetcher from "../src/fetcher";
import NavBar from "./navbar";
import SideDrawer from "./sideDrawer";

interface MainContainerProps {
  children: any;
}

const MainContainer: FC<MainContainerProps> = ({ children }) => {
  const { data: session, status } = useSession();
  const { data, error } = useSWR(() => `/api/users/${session!.id}`, fetcher);
  const isLoading = status === "loading" || (!data && !error);

  const drawerWidth = {
    sm: 100,
    md: 240,
  };

  return (
    <>
      {isLoading ? (
        <NavBar leftMargin={drawerWidth.md} isLoading />
      ) : (
        <NavBar leftMargin={drawerWidth.md} profile={data} />
      )}
      {isLoading ? (
        <SideDrawer width={drawerWidth.md} isLoading />
      ) : (
        <SideDrawer width={drawerWidth.md} profile={data} />
      )}
      <Box
        sx={{
          minHeight: "100vh",
          backgroundColor: alpha("#b2dfdb", 0.05),
        }}
      >
        <Container maxWidth="lg">
          <Box
            component={"main"}
            sx={{
              flexGrow: 1,
              marginLeft: `${drawerWidth.md}px`,
              paddingTop: 8,
            }}
          >
            {children}
          </Box>
        </Container>
      </Box>
    </>
  );
};

export default MainContainer;
