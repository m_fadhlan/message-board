import Image from "next/image";
import Avatar from "boring-avatars";
import React from "react";
import type { Profile } from "../types/interfaces";

interface AvatarIconProps {
  width: number;
  profile: Profile;
}

function addStr(str:string, index:number, stringToAdd:string){
  return str.substring(0, index) + stringToAdd + str.substring(index, str.length);
}

const customCloudinaryLoader = ({
  src,
  width,
}: {
  src: string;
  width: number;
}) => {
  const options = `ar_1:1,c_fill,w_${width.toString()}/r_${width.toString()}/`
  return addStr(src, 48, options)
};

const AvatarIcon = ({ width, profile }: AvatarIconProps) => {
  const profileName = (profile.name ? profile.name : profile.username);

  if (profile.profile_pic) {
    return (
      <Image
        src={profile.profile_pic}
        width={width}
        height={width}
        alt={profileName}
        loader={customCloudinaryLoader}
      />
    );
  } else {
    return (
      <Avatar
        size={`${width.toString()}px`}
        name={profileName}
        variant="marble"
        colors={["#B2DFDB", "#E5FFFF", "#82ADA9", "#FFDDC1", "#FFAB91"]}
      />
    );
  }
};

export default AvatarIcon;
