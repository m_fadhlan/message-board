import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  Avatar,
  IconButton,
  CardActions,
  Button,
  Skeleton,
} from "@mui/material";
import { red } from "@mui/material/colors";
import { HiDotsVertical } from "@react-icons/all-files/hi/HiDotsVertical";
import Link from "next/link";
import React from "react";
import useSWR from "swr";
import dateConverter from "../src/dateConverter";
import fetcher from "../src/fetcher";
import { Post } from "../types/interfaces";
import AvatarIcon from "./avatarIcon";

interface CardPostProps {
  isLoading?: boolean;
  post?: Post;
}

const CardPost = (props: CardPostProps) => {
  const { data, error } = useSWR(
    () => `/api/users/${props.post!.owner}`,
    fetcher
  );
  const isLoading = props.isLoading || (!data && !error);

  return (
    <>
      <Card
        elevation={0}
        sx={{
          minWidth: 275,
          border: 1,
          borderColor: "grey.300",
          my: 4,
        }}
      >
        <Link href={isLoading ? "" : (`/profile/${data.username}`)}>
          <CardHeader
            sx={[
              {
                '&:hover': {
                  cursor: 'pointer'
                },
              },
            ]}
            avatar={
              isLoading ? (
                <Skeleton
                  animation="pulse"
                  variant="circular"
                  width={40}
                  height={40}
                />
              ) : (
                <AvatarIcon profile={data} width={48} />
              )
            }
            title={
              isLoading ? (
                <Skeleton
                  height={10}
                  animation="pulse"
                  width="50%"
                  sx={{ marginBottom: 0.5 }}
                />
              ) : (
                data.name ? data.name : data.username
              )
            }
            subheader={
              isLoading ? (
                <Skeleton height={10} animation="pulse" width="30%" />
              ) : (
                "@" + data.username
              )
            }
          />
        </Link>
        <CardContent
          sx={{
            px: 2.5,
          }}
        >
          <Typography
            variant="h5"
            sx={{
              fontWeight: "medium",
            }}
          >
            {isLoading ? (
              <Skeleton animation="pulse" width="50%" />
            ) : (
              props.post?.title
            )}
          </Typography>
          <Typography sx={{ mb: 1.5 }} color="text.secondary">
            {isLoading ? (
              <Skeleton animation="pulse" width="30%" />
            ) : (
              dateConverter(props.post?.updated_at)
            )}
          </Typography>
          {isLoading ? (
            <Skeleton animation="pulse" variant="rectangular" height={118} />
          ) : (
            <Typography variant="body2">{props.post?.body}</Typography>
          )}
        </CardContent>
        <CardActions
          sx={{
            px: 2.5,
            paddingBottom: 1,
          }}
        >
          {isLoading ? (
            <></>
          ) : (
            <Link href={`/messages/${props.post!.id}`}>
            <Button size="small" color="primary">
              Lebih Lanjut
            </Button>
            </Link>
          )}
        </CardActions>
      </Card>
    </>
  );
};

export default CardPost;
