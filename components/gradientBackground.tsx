import { EmotionJSX } from "@emotion/react/types/jsx-namespace";
import { Box } from "@mui/material";
import React from "react";

interface GradientBackgroundProps {
  children: React.ReactNode;
}

const GradientBackground = ({ children }: GradientBackgroundProps) => {
  return (
    <Box
      sx={{
        background: "linear-gradient(203.94deg, #FFAB91 -29.13%, #65C3C8 111.04%)",
      }}
    >
      {children}
    </Box>
  );
};

export default GradientBackground;
